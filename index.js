
import PagePack from "flypack";
import ES from "flypack/es/index.mjs";
import FlyPackUtil from "flypack/pack/index.js"
import path from "path";

import FlyProject from "flyproject";

import fs from "fs";

export default class FlyPack {
  static async run() {
    try {
      let _this = new FlyPack();

      const app_path = process.cwd();
      const app_cfg_path = path.resolve(app_path, "mellisuga.json");
      
      let app_cfg = undefined; 
      try {
        app_cfg = JSON.parse(fs.readFileSync(app_cfg_path, 'utf8'));
      } catch (e) {
        console.log("FAILED TO PARSE", app_cfg_path);
        console.error(e.stack);
      }

      const mellisuga_modules_path = path.resolve(app_path, "mellisuga_modules");
      const command = process.argv[4];


      switch (command) {
        case "watch":





          if (app_cfg) {

            console.log("WATCHING:", path.resolve(process.cwd(), "pages"));
            let pagepack = await PagePack.init(undefined, {
              app_path: app_path,
      /*        aliases: {
                core: path.resolve(app_path, "mellisuga_modules", "web", "core"),
                mellisuga: path.resolve(app_path, "mellisuga_modules", "web", "core"),
                community: path.resolve(app_path, "mellisuga_modules", "web", "community")
              },*/
              dev_mode: true
            });

            let page_list = pagepack.serve_dirs(undefined, path.resolve(app_path, 'pages'), {
              globals_path: path.resolve(app_path, "globals")
            });
      /*      if (app_cfg.webpack) {
              for (let p = 0; p < app_cfg.webpack.length; p++) {
                let pac_module = app_cfg.webpack[p];
                let src_path = path.resolve(app_path, pac_module.src);
                let out_path = path.resolve(app_path, pac_module.out);
                let out_dir_path = path.dirname(out_path);
                let flypack = new FlyPackUtil(out_dir_path, pagepack, {
                  app_path: app_path,
                  webpack_src: src_path,
                  webpack_out: out_path,
                  dev_mode: true
                });
                flypack.toggle_webpack_watch();
              }
            }*/

            if (app_cfg.flypack) {
              for (let p = 0; p < app_cfg.flypack.length; p++) {
                let sub_list_path = app_cfg.flypack[p];
                console.log("WATCHING:", path.resolve(process.cwd(), sub_list_path));
                let sub_list = pagepack.serve_dir(undefined, path.resolve(app_path, sub_list_path), {
                  globals_path: path.resolve(app_path, "globals")
                });
              }
            }

            function watch_module_pages(modir_path) {
              if (fs.existsSync(modir_path)) {
                let waf_core_modules = fs.readdirSync(modir_path);
                for (let m = 0; m < waf_core_modules.length; m++) {
                  let module_name = waf_core_modules[m];
                  let module_json = JSON.parse(fs.readFileSync(path.resolve(modir_path, module_name, "module.json"), 'utf8'));
                  if (module_json.pagepack) {
                    for (let p = 0; p < module_json.pagepack.length; p++) {
                      let pack = module_json.pagepack[p];
                      console.log("WATCH", path.resolve(modir_path, module_name, pack));
                      let page_list = pagepack.serve_dir(undefined, path.resolve(modir_path, module_name, pack), {
                        globals_path: path.resolve(app_path, "globals")
                      }); 
                    }
                  }
                }
                
              }
            }
            watch_module_pages(path.resolve(mellisuga_modules_path, "waf/core"));
            watch_module_pages(path.resolve(mellisuga_modules_path, "waf/community"));
          }

          

          break;
        case "produce":
          if (app_cfg) {
            const flyproject = new FlyProject(app_path);
            let contexts = [flyproject.globals, ...flyproject.pages.list];

            for (const context of contexts) {
              await ES.construct(context.full_path, {
                app_path: app_path
              });
              await ES.construct(context.full_path, {
                app_path: app_path,
                legacy: true
              });
            }

            
          }
          break;
        default:
          console.log("PagePack: invalid command `"+command+"`!");
          process.exit();
      }
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }
}
